#from fnsapi.api import FNSApi

import importlib, importlib.util

def module_from_file(module_name, file_path):
    spec = importlib.util.spec_from_file_location(module_name, file_path)
    module = importlib.util.module_from_spec(spec)
    spec.loader.exec_module(module)
    return module

api = module_from_file("FNSApi","../fnsapi/api.py")

fns_api = api.FNSApi()
result = fns_api.get_session_token()
print(result)

session_token = result['token']
user_id = "12345"
#get_ticket(self, session_token, user_id, sum, timestamp, fiscal_number, operation_type, fiscal_document_id, fiscal_sign):
amount = 47872
date = '2021-10-17T19:49:00'
fnumber = '9280440301246196'
fdocid = '120729'
fsign = '1827598198'

result = fns_api.get_ticket(session_token, user_id, amount, date, fnumber, 1, fdocid, fsign)
print(result)



def test_get_token(self):
    fns_api = FNSApi()
    result = fns_api.get_session_token()
