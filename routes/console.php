<?php

use Illuminate\Foundation\Inspiring;
use Illuminate\Support\Facades\Artisan;
use App\FNSValidator;

/*
|--------------------------------------------------------------------------
| Console Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of your Closure based console
| commands. Each Closure is bound to a command instance allowing a
| simple approach to interacting with each command's IO methods.
|
*/

Artisan::command('check:test', function () {
    //$check = \App\Entities\Check\Models\Check::first();
    //\App\Entities\Check\Helpers\FNSValidator::validate($check);

    $master_token = config('services.fns.master_token');
    $master_token_base_64 = base64_encode('1234');

    $amount = 47872;
    $date = '2021-10-17T19:49:00';
    $fnumber = '9280440301246196';
    $fdocid = '120729';
    $fsign = '1827598198';

    $check_data = compact('amount', 'date', 'fnumber', 'fdocid', 'fsign');

    $this->info("Preparing auth request");
    $body = FNSValidator::createAuthRequest($master_token);
    $this->info($body);
    $ch = curl_init();
    curl_setopt_array($ch, array(
        CURLOPT_URL => "https://openapi.nalog.ru:8090/open-api/AuthService/0.1?wsdl",
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => "",
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => 0,
        CURLOPT_FOLLOWLOCATION => true,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => "POST",
        CURLOPT_POSTFIELDS => $body,
        CURLOPT_HTTPHEADER => ["Content-Type: text/xml"],
    ));

    $response = curl_exec($ch);
    $error = curl_error($ch);
    curl_close($ch);
    $this->info("Auth response: ");
    $this->info(html_entity_decode($response));
    $this->info("Auth error: ");
    $this->info($error);

    $this->info("Parsing response for temporary token");
    $temp_token = null;
    $dom = new DOMDocument();
    $dom->loadXML($response);
    foreach ($dom->getElementsByTagName('Token') as $element) {
        $temp_token = $element->nodeValue;
    }

    $this->info('Temp token: ' . $temp_token);

    if (is_null($temp_token)) {
        $this->info("Temp token not found");
        return;
    }

    $this->info("Preparing GetCheckInfo request");
    $body = FNSValidator::createGetCheckInfoRequest($check_data);
    $this->info($body);

    $ch = curl_init();

    curl_setopt_array($ch, array(
        CURLOPT_URL => "https://openapi.nalog.ru:8090/open-api/ais3/KktService/0.1?wsdl",
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => "",
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => 0,
        CURLOPT_FOLLOWLOCATION => true,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => "POST",
        CURLOPT_POSTFIELDS => $body,
        CURLOPT_HTTPHEADER => [
            "FNS-OpenApi-Token: $temp_token",
            //"FNS-OpenApi-UserToken: $master_token",
            "FNS-OpenApi-UserToken: $master_token_base_64",
            "Content-Type: text/xml"
        ],
    ));

    $response = curl_exec($ch);
    $error = curl_error($ch);
    curl_close($ch);
    $this->info("GetCheckInfo response: ");
    $this->info(html_entity_decode($response));
    $this->info("GetCheckInfo error: ");
    $this->info($error);

    $dom = new DOMDocument();
    $dom->loadXML($response);
    $message_id = null;
    foreach ($dom->getElementsByTagName('MessageId') as $element) {
        $message_id = $element->nodeValue;
    }

    $this->info('Message ID from response: ');
    $this->info($message_id ?? 'message_id not found in response');

    if (is_null($message_id)) return;

    $needs_validate = true;
    while ($needs_validate) {
        $n=10;
        $this->info("sleeping $n sec...");
        sleep($n);

        $this->info("Preparing get message request");
        $body = FNSValidator::createGetMessageRequest($message_id);
        $this->info($body);

        $ch = curl_init();

        curl_setopt_array($ch, array(
            CURLOPT_URL => "https://openapi.nalog.ru:8090/open-api/ais3/KktService/0.1?wsdl",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POSTFIELDS => $body,
            CURLOPT_HTTPHEADER => [
                "FNS-OpenApi-Token: $temp_token",
                "FNS-OpenApi-UserToken: $master_token_base_64",
                //"FNS-OpenApi-UserToken: $master_token",
                "Content-Type: text/xml"
            ],
        ));

        $response = curl_exec($ch);
        $error = curl_error($ch);
        curl_close($ch);
        $this->info("GetMessage response: ");
        $this->info(html_entity_decode($response));
        $this->info("GetMessage error: ");
        $this->info($error);

        $dom = new DOMDocument();
        $dom->loadXML($response);

        foreach ($dom->getElementsByTagName('Code') as $element) {
            $code = $element->nodeValue;
        }
        $code = $code ?? 'Not found in response';
        foreach ($dom->getElementsByTagName('Ticket') as $element) {
            $message = $element->nodeValue;
        }
        $message = $message ?? 'Not found in response';
        $this->info('Code: ' . $code);
        $this->info('Message: ' . $message);
    }

});
