<?php

namespace App;

use App\Entities\Check\Models\Check;
use Carbon\Carbon;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Log;

class FNSValidator
{
    public static function validate(?Check $check = null)
    {
        $master_token = config('services.fns.master_token');
        $body = "
            <soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:ns=\"urn://x-artefacts-gnivc-ru/inplat/servin/OpenApiMessageConsumerService/types/1.0\">
            <soapenv:Header/>
            <soapenv:Body>
            <ns:GetMessageRequest>
            <ns:Message>\n<tns:AuthRequest xmlns:tns=\"urn://x-artefacts-gnivc-ru/ais3/kkt/AuthService/types/1.0\">
            <tns:AuthAppInfo>
            <tns:MasterToken>$master_token</tns:MasterToken>
            </tns:AuthAppInfo>
            </tns:AuthRequest>
            </ns:Message>
            </ns:GetMessageRequest>
            </soapenv:Body>
            </soapenv:Envelope>";
        $response = Http::withBody($body, 'text/xml')->post('https://openapi.nalog.ru:8090/open-api/AuthService/0.1?wsdl');
        Log::channel('fns')->debug('Auth response: ' . $response->status());
        Log::channel('fns')->debug($response->body());
        $dom = new \DOMDocument();
        $dom->loadXML($response->body());
        $temp_token = null;
        $expire_at = null;
        foreach ($dom->getElementsByTagName('Token') as $element) {
            $temp_token = $element->nodeValue;
        }
        foreach ($dom->getElementsByTagName('ExpireTime') as $element) {
            $expire_at = Carbon::parse($element->nodeValue);
        }

        if (is_null($temp_token)) {
            // we have to set error state and queue this check to later time
            Log::channel('fns')->debug('Temporary token not found in response');
            return;
        }
        Log::channel('fns')->debug('Master token: ' . $master_token);
        Log::channel('fns')->debug('Master token base64: ' . base64_encode($master_token));
        Log::channel('fns')->debug('Temporary token: ' . $temp_token);
        Log::channel('fns')->debug('Expire at: ' . $expire_at->toDateTimeString());

        $amount = 47872;
        $date = '2021-10-17T19:49';
        $fnumber = '9280440301246196';
        $fdocid = '120729';
        $fsign = '1827598198';
        $body = "<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\"
                 xmlns:ns=\"urn://x-artefacts-gnivc-ru/inplat/servin/OpenApiAsyncMessageConsumerService/types/1.0\">
                <soapenv:Header/>
                <soapenv:Body>
                    <ns:SendMessageRequest>
                        <ns:Message>
                            <tns:GetTicketRequest xmlns:tns=\"urn://x-artefacts-gnivc-ru/ais3/kkt/KktTicketService/types/1.0\">
                                <tns:GetTicketInfo>
                                    <tns:Sum>$amount</tns:Sum>
                                    <tns:Date>$date</tns:Date>
                                    <tns:Fn>$fnumber</tns:Fn>
                                    <tns:TypeOperation>1</tns:TypeOperation>
                                    <tns:FiscalDocumentId>$fdocid</tns:FiscalDocumentId>
                                    <tns:FiscalSign>$fsign</tns:FiscalSign>
                                </tns:GetTicketInfo>
                            </tns:GetTicketRequest>
                        </ns:Message>
                    </ns:SendMessageRequest>
                </soapenv:Body>
            </soapenv:Envelope>";

        $response = Http::withBody($body, 'text/xml')
            ->withHeaders([
                'FNS-OpenApi-Token: ' . $temp_token,
                'FNS-OpenApi-UserToken: ' . base64_encode($master_token),
                'Content-Type: text/xml',
            ])
            ->post('https://openapi.nalog.ru:8090/open-api/ais3/KktService/0.1?wsdl');

        Log::channel('fns')->debug('Check request response: ' . $response->status());
        Log::channel('fns')->debug(html_entity_decode($response->body()));


        /*
        ->header('FNS-OpenApi-Token: ' . $temp_token)
        ->header('FNS-OpenApi-UserToken: ' . $master_token)
        */

    }

    public static function createAuthRequest(string $master_token)
    {
        return
            "<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:ns=\"urn://x-artefacts-gnivc-ru/inplat/servin/OpenApiMessageConsumerService/types/1.0\">
            <soapenv:Header/>
            <soapenv:Body>
            <ns:GetMessageRequest>
            <ns:Message>\n<tns:AuthRequest xmlns:tns=\"urn://x-artefacts-gnivc-ru/ais3/kkt/AuthService/types/1.0\">
            <tns:AuthAppInfo>
            <tns:MasterToken>$master_token</tns:MasterToken>
            </tns:AuthAppInfo>
            </tns:AuthRequest>
            </ns:Message>
            </ns:GetMessageRequest>
            </soapenv:Body>
            </soapenv:Envelope>";
    }

    public static function createGetCheckInfoRequest(array $params)
    {
        $format = "
            <soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\"
                         xmlns:ns=\"urn://x-artefacts-gnivc-ru/inplat/servin/OpenApiAsyncMessageConsumerService/types/1.0\">
            <soapenv:Header/>
            <soapenv:Body>
                <ns:SendMessageRequest>
                    <ns:Message>
                        <tns:GetTicketRequest xmlns:tns=\"urn://x-artefacts-gnivc-ru/ais3/kkt/KktTicketService/types/1.0\">
                            <tns:GetTicketInfo>
                                <tns:Sum>%s</tns:Sum>
                                <tns:Date>%s</tns:Date>
                                <tns:Fn>%s</tns:Fn>
                                <tns:TypeOperation>1</tns:TypeOperation>
                                <tns:FiscalDocumentId>%s</tns:FiscalDocumentId>
                                <tns:FiscalSign>%s</tns:FiscalSign>
                            </tns:GetTicketInfo>
                        </tns:GetTicketRequest>
                    </ns:Message>
                </ns:SendMessageRequest>
            </soapenv:Body>
        </soapenv:Envelope>";

        return sprintf($format,
            Arr::get($params, 'amount'),
            Arr::get($params, 'date'),
            Arr::get($params, 'fnumber'),
            Arr::get($params, 'fdocid'),
            Arr::get($params, 'fsign')
        );
    }

    public static function createGetMessageRequest(string $message_id)
    {
        return "
            <soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:ns=\"urn://x-artefacts-gnivc-ru/inplat/servin/OpenApiAsyncMessageConsumerService/types/1.0\">
               <soapenv:Header/>
               <soapenv:Body>
                  <ns:GetMessageRequest>
                     <ns:MessageId>$message_id</ns:MessageId>
                  </ns:GetMessageRequest>
               </soapenv:Body>
            </soapenv:Envelope>";
    }
}
